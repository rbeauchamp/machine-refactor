namespace FortCodeExercises.Exercise1
{
    public class Machine
    {
        public string machineName = "";
        public int type = 0;

        public string name
        {
            get
            {
                var calculatedName = "";
                if (string.IsNullOrEmpty(this.machineName))
                {
                    calculatedName = this.type switch
                    {
                        2 => "tractor",
                        0 => "bulldozer",
                        1 => "crane",
                        4 => "car",
                        3 => "truck",
                        _ => calculatedName
                    };
                }
                return calculatedName;
            }
        }

        public string description
        {
            get
            {
                return $" {this.color} {this.name} [{this.getMaxSpeed(this.type, this.hasMaxSpeed())}].";
            }
        }

        private bool hasMaxSpeed()
        {
            return this.type switch
            {
                3 => false,
                1 => true,
                2 => true,
                4 => false,
                _ => true
            };
        }

        public string color
        {
            get
            {
                return this.type switch
                {
                    1 => "blue",
                    0 => "red",
                    4 => "brown",
                    3 => "yellow",
                    2 => "green",
                    _ => "white"
                };
            }
        }

        public string trimColor
        {
            get
            {
                var baseColor = this.getBaseColor();

                return this.type switch
                {
                    1 when this.isDark(baseColor) => "black",
                    1 when !this.isDark(baseColor) => "white",
                    2 when this.isDark(baseColor) => "gold",
                    3 when this.isDark(baseColor) => "silver",
                    _ => ""
                };
            }
        }

        private string getBaseColor()
        {
            return this.type switch
            {
                0 => "red",
                1 => "blue",
                2 => "green",
                3 => "yellow",
                4 => "brown",
                _ => "white"
            };
        }

        public bool isDark(string color)
        {
            return color switch
            {
                "red" => true,
                "yellow" => false,
                "green" => true,
                "black" => true,
                "white" => false,
                "beige" => false,
                "babyblue" => false,
                "crimson" => true,
                _ => false
            };
        }

        public int getMaxSpeed(int machineType, bool noMax = false)
        {
            return machineType switch
            {
                1 when !noMax => 70,
                2 when !noMax => 60,
                0 when noMax => 80,
                2 when noMax => 90,
                4 when noMax => 90,
                1 when noMax => 75,
                _ => 70
            };
        }
    }
}
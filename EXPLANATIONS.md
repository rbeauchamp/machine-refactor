# Machine class refactorings
The non-breaking refactorings below increased the Machine class's overall maintainbility index from 62 -> 87 and decreased its cyclomatic complexity from 55 -> 9.

## name property
- change name of local variable `machineName` to `calculatedName` to avoid potential confusion with class field name
- improve readability by using `string.IsNullOrEmpty()` rather than `(this.machineName == null || this.machineName == "")`
- make code more succinct and maintainable by replacing if/else statements with switch expression
- above changes increased the maintainbility index from 63 -> 73 and decreased cyclomatic complexity from 8 -> 2

## description property
- improve readability, maintainability, and performance by replacing string concatentation with string interpolation
- extract calculation of hasMaxSpeed into separate method to simplify implementation
- make code more succinct and maintainable by replacing hasMaxSpeed if/else statements with switch expression
- above changes increased the maintainbility index from 58 -> 90 and decreased cyclomatic complexity from 5 -> 1

## color property
- eliminate redundant initialization of local variable
- make code more succinct and maintainable by replacing if/else statements with switch expression
- above change increased the maintainbility index from 65 -> 88 and decreased cyclomatic complexity from 6 -> 1

## trimColor property
- eliminate redundant initialization of local variable
- extract calculation of baseColor into separate method to simplify implementation
- make code more succinct and maintainable by replacing if/else statements with switch expression
- above changes increased the maintainbility index from 58 -> 90 and decreased cyclomatic complexity from 5 -> 1

## isDark() method
- simplify implementation by eliminating local `isDark` variable and converting if/else statements to switch expression
- above change increased the maintainbility index from 61 -> 87 and decreased cyclomatic complexity from 9 -> 1

## getMaxSpeed() method
- remove unused local variable `absoluteMax`
- remove redundant bool comparisons such as `noMax == true`
- convert if/else statements to switch expression
- above changes increased the maintainbility index from 58 -> 76 and decreased cyclomatic complexity from 13 -> 1